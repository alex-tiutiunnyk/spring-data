package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        //not ideal, but working :)
        List<Technology> tech1 = technologyRepository.findIdByName(oldTechnologyName);
        Technology techElement1 = tech1.get(0);
        UUID oldTechName = techElement1.getId();

        List<Technology> tech2 = technologyRepository.findIdByName(newTechnologyName);
        Technology techElement2 = tech2.get(0);
        UUID newTechName = techElement2.getId();

        int[] countUser = teamRepository.countUser();
        for (int j : countUser) {
            if (j < devsNumber) {
                teamRepository.updateTechnology(oldTechName, newTechName);
            }
        }
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
