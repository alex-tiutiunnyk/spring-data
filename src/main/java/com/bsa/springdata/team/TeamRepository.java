package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String name);

    @Modifying
    @Transactional
    @Query(value = "update teams t\n" +
            "set name = concat(?1, '_', p.name, '_', tech.name)\n" +
            "from technologies tech, projects p\n" +
            "where t.name = ?1 and t.technology_id = tech.id and t.project_id = p.id",
            nativeQuery = true)
    void normalizeName(String name);

    @Query(value = "select count(u.id) as count\n" +
            "from users u, teams t\n" +
            "where u.team_id = t.id\n" +
            "group by team_id",
            nativeQuery = true)
    int[] countUser();

    @Modifying
    @Transactional
    @Query(value = "update teams t\n" +
            "set technology_id = ?2\n" +
            "where t.technology_id = ?1",
            nativeQuery = true)
    void updateTechnology(UUID oldTechnologyName, UUID newTechnologyName);

    long countByTechnologyName(String technologyName);
}
