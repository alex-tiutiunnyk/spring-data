package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology, UUID> {
    //method for RoleService (deleteByRoleCode)
    Technology findFirstByName(String name);

    Optional<Technology> findByName(String name);

    //method for TeamService (updateTechnology)
    List<Technology> findIdByName(String techName);
}