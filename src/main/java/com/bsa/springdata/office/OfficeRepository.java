package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query(
            value = "select o.*\n" +
                    "      from technologies t, projects p, teams, users u, offices o\n" +
                    "      where teams.project_id = p.id\n" +
                    "        and teams.technology_id = t.id\n" +
                    "        and u.team_id = teams.id\n" +
                    "        and t.name = ?1 and o.id = u.office_id\n" +
                    "group by o.id",
            nativeQuery = true)
    List<Office> getOfficeByTechnology(String technology);

    @Modifying
    @Transactional
    @Query(value = "update offices o\n" +
            "set address = ?2\n" +
            "from users u\n" +
            "where address = ?1 and u.office_id = o.id and u.office_id = o.id",
            nativeQuery = true)
    void update(String oldAddress, String newAddress);

    List<Office> getOfficeByAddress(String newAddress);
}
