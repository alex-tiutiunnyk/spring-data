package com.bsa.springdata.role;

import com.bsa.springdata.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public void deleteRole(String roleCode) {
        if (userRepository.countByRoles_Code(roleCode) == 0)
            roleRepository.deleteByCode(roleCode);
    }
}
