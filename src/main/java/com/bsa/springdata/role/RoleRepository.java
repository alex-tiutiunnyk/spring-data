package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {
    long deleteByCode(String roleCode);

//    Second variant and it doesn't it work. Why?
//    @Query("SELECT p FROM Role p JOIN FETCH p.users WHERE p.code = (:code)")
//    Role findByCodeAndFetchUsersEagerly(@Param("code") String code);

//      or this?
//    Role getOneByCodeWithUsers(String roleCode);
//    Role getOneByCode(String roleCode);
}
