package com.bsa.springdata.project.dto;

import org.springframework.stereotype.Repository;

// TODO: Use this interface when you make a projection from native query.
//  If you don't use native query replace this interface with a simple POJO
@Repository
public interface ProjectSummaryDto {
    String getName();

    long getTeamsNumber();

    long getDevelopersNumber();

    String getTechnologies();
}