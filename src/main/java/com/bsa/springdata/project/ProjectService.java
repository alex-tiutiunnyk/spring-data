package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;


    public List<ProjectDto> findTop5ByTechnology(String technology) {
        Technology tech = technologyRepository.findFirstByName(technology);
        return projectRepository.findByTeams_TechnologyId(tech.getId(), PageRequest.of(0, 5))
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        List<Project> projects = projectRepository.getAll(PageRequest.of(0, 1));
        return projects.size() == 0 ? Optional.empty() : Optional.of(ProjectDto.fromEntity(projects.get(0)));
    }

    //not done
    public List<ProjectSummaryDto> getSummary() {
        return null;
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
    }

    public int getCountWithRole(String role) {
        return projectRepository.getProjectByRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {

        var createdProject = new Project();
        createdProject.setName(createProjectRequest.getProjectName());
        createdProject.setDescription(createProjectRequest.getProjectDescription());

        var createdTeam = new Team();
        createdTeam.setArea(createProjectRequest.getTeamArea());
        createdTeam.setRoom(createProjectRequest.getTeamRoom());
        createdTeam.setName(createProjectRequest.getTeamName());

        var createdTech = new Technology();
        createdTech.setName(createProjectRequest.getTech());
        createdTech.setDescription(createProjectRequest.getTechDescription());
        createdTech.setLink(createProjectRequest.getTechLink());

        createdTeam.setTechnology(createdTech);
        createdTeam.setProject(createdProject);
        createdProject.setTeams(createdProject.getTeams());

        projectRepository.save(createdProject);
        technologyRepository.save(createdTech);
        teamRepository.save(createdTeam);

        return createdProject.getId();
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
    }
}
