package com.bsa.springdata.project;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query(
            value = "select p.*,  count(t.id) as userCount\n" +
                    "    from  projects p, teams t, users u, technologies tech\n" +
                    "where u.team_id = t.id\n" +
                    "  and p.id = t.project_id\n" +
                    "  and t.technology_id = tech.id\n" +
                    "  and tech.id = ?1 \n" +
                    "group by p.id\n" +
                    "order by  userCount desc",
            nativeQuery = true)
    List<Project> findByTeams_TechnologyId(UUID technologyId, Pageable pageable);

    @Query(
            value = "select distinct p2.*,\n" +
                    "                projectQuery.teamCount,\n" +
                    "                countTotal.userCount\n" +
                    "from projects p2,\n" +
                    "              (select p.id,  count(t.id) as teamCount\n" +
                    "               from  teams t,projects p\n" +
                    "               where t.project_id = p.id\n" +
                    "               group by p.id) as projectQuery,\n" +
                    "       (select count(t.id) as userCount\n" +
                    "            from\n" +
                    "                teams t, users u\n" +
                    "             where u.team_id = t.id\n" +
                    "            group by t.id) as countTotal\n" +
                    "where projectQuery.id = p2.id\n" +
                    "order by teamCount desc, userCount desc, p2.name desc",
            nativeQuery = true)
    List<Project> getAll(Pageable pageable);

    @Query(
            value = "select  count(p.id) from projects p,\n" +
                    "              (select t.project_id, count(u.id) as userCount\n" +
                    "               from  teams t ,users u,user2role u2r, roles r\n" +
                    "               where u.team_id = t.id\n" +
                    "                 and u2r.user_id = u.id and u2r.role_id = r.id\n" +
                    "                 and r.name = ?1\n" +
                    "               group by  r.id, t.project_id) as subQuery\n" +
                    "where p.id = (subQuery.project_id) and subQuery.userCount > 0",
            nativeQuery = true)
    int getProjectByRole(String role);
}